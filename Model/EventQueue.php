<?php

namespace Webgears\Bundle\EventBundle\Model;

class EventQueue
{
    protected static ?EventQueue $instance = null;
    protected array $event_queue = [];

    public static function getInstance(): self
    {
        if (null === self::$instance) {
            self::$instance = new EventQueue();
        }

        return self::$instance;
    }

    private function __construct() {}

    /**
     * Add a new event
     */
    public function push(ActionEvent $event): self
    {
        $this->event_queue[] = $event;

        return $this;
    }

    /**
     * Return all event
     *
     * @return ActionEvent[]
     */
    public function getAll(): array
    {
        return $this->event_queue;
    }

    /**
     * Return the next event in the queue
     */
    public function pop(): ?ActionEvent
    {
        return array_shift($this->event_queue);
    }
}
