<?php

namespace Webgears\Bundle\EventBundle\Model;

use function call_user_func_array;

class CallableEvent extends ActionEvent
{
    /** @var callable */
    protected $service;

    /** @var array */
    protected $data;

    public function __construct(callable $service, array $data)
    {
        $this->service = $service;
        $this->data = $data;
    }

    public function action(): void
    {
        call_user_func_array($this->service, $this->data);
    }
}