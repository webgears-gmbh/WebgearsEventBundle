<?php

namespace Webgears\Bundle\EventBundle\Model;

use Symfony\Contracts\EventDispatcher\Event as BaseEvent;

abstract class ActionEvent extends BaseEvent
{
    public const NAME = 'webgears.event_bundle.event';

    abstract public function action();
}